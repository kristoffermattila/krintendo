#ifndef RICOH_6502_H
#define RICOH_6502_H

class Ricoh_6502
{
private:
	unsigned char A{0}, X{0}, Y{0}, P{0}, S{0};
	unsigned short PC{0};

public:
	void tick();

	unsigned short getPC() { return PC; }
};

#endif