#include "gtest/gtest.h"
#include "src/cpu/Ricoh_6502.h"

TEST(Cputest, Ticking)
{
	Ricoh_6502 cpu{};
	ASSERT_EQ(cpu.getPC(), 0);
	cpu.tick();
	ASSERT_EQ(cpu.getPC(), 1);
}